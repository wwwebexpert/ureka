module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "globals" : {
        "window" : true,
        "document" : true,
        "it" : true
    },
    "rules": {
     "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    }
};