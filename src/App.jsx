import React from 'react';
import './App.css';
import Footer from './footer/Footer';
import Customers from './customers/Customers';
import ThemeFeatures from './themeFeatures/ThemeFeatures';
import Subscription from './subscription/Subscription';
import Features from './features/Features';
import Header from './header/Header';
import Home from './home/Home';

function App() {
  return (
    <div className="App">
      <Header />
      <Home />
      <Features />
      <Subscription />
      <ThemeFeatures />
      <Customers />
      <Footer />
    </div>
  );
}

export default App;
