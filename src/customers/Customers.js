import React from 'react';

import CustomerImage from './customerImage/CustomerImage';

function Customers() {
  return (
    <div className="clients-section">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>Our Customers</h3>
            <p>These are some of our customers who have helped us by using our product.</p>
            <div className="logos">
              <CustomerImage imgLink="images/logos/google.png" imgAlt="Google" />
              <CustomerImage imgLink="images/logos/facebook.png" imgAlt="Facebook" />
              <CustomerImage imgLink="images/logos/apple.png" imgAlt="Apple" />
              <CustomerImage imgLink="images/logos/stripe.png" imgAlt="Stripe" />
              <CustomerImage imgLink="images/logos/yahoo.png" imgAlt="Yahoo" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Customers;
