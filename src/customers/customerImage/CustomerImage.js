import React from 'react';

function CustomerImage(props) {
  return (
    <img src={props.imgLink} alt={props.imgAlt} />
  );
}

CustomerImage.propTypes = {
  imgLink: React.PropTypes.string.isRequired,
  imgAlt: React.PropTypes.string.isRequired,
};
export default CustomerImage;
