import React from 'react';
import Header from './header/Header';
import TopData from './topData/TopData';
import BottomData from './bottomData/BottomData';

function Features() {
  return (
    <div>
      <div className="features-section">
        <div className="container">
          <Header />
          <TopData />
          <div className="divider" />
          <BottomData />
        </div>
      </div>
    </div>
  );
}

export default Features;
