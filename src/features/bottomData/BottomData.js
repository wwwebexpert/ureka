import React from 'react';

function BottomData() {
  return (
    <div>
      <div className="row feature backwards">
        <div className="col-md-6 info">
          <h4>A fully featured well design template that works.</h4>
          <p>You have complete control over the look & feel of your website,
             we offer the best quality so you take your site up and running in no time.
                        </p>
          <p>Write some text here to explain the
            features of your site or application, it
                            has lots of uses.
                        </p>
        </div>
        <div className="col-md-6 image">
          <img src="images/feature2.png" className="img-responsive" alt="feature2" />
        </div>
      </div>
    </div>
  );
}

export default BottomData;
