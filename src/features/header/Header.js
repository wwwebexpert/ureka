import React from 'react';

function Header() {
  return (
    <div>
      <div className="row header">
        <div className="col-md-12">
          <h2>Need an easy way to customize your site?</h2>
          <p>React is perfect for novice developers and experts alike.</p>
        </div>
      </div>
    </div>
  );
}

export default Header;
