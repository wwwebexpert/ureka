import React from 'react';

function TopData() {
  return (
    <div>
      <div className="row feature">
        <div className="col-md-6 info">
          <h4>You do not need to have great technical experience to use your product.</h4>
          <p>Whether you want to fill this paragraph with some text
             like I am doing right now,
             this place is perfect to describe some features or anything
             you want - React has a complete solution for you.
                    </p>
        </div>
        <div className="col-md-6 image">
          <img src="images/feature1.png" className="img-responsive" alt="feature1" />
        </div>
      </div>
    </div>
  );
}

export default TopData;
