import React from 'react';

import Links from './links/Links';
import Newsletter from './newsletter/Newsletter';
import Copyrights from './copyrights/Copyrights';

function Footer() {
  const overviewLinkItems = [
    {
      name: 'Features',
      url: 'features.html',
    },
    {
      name: 'Services',
      url: 'services.html',
    },
    {
      name: 'Pricing',
      url: 'pricing.html',
    },
    {
      name: 'Support',
      url: 'support.html',
    },
    {
      name: 'Blog',
      url: 'blog.html',
    },
    {
      name: 'Coming Soon',
      url: 'blog.html',
    },
  ];
  const menuLinkItems = [
    {
      name: 'About us',
      url: 'features.html',
    },
    {
      name: 'Contact us',
      url: 'services.html',
    },
    {
      name: 'Jobs',
      url: 'pricing.html',
    },
    {
      name: 'Portfolio',
      url: 'support.html',
    },
    {
      name: 'Status',
      url: 'status.html',
    },
  ];

  const socialLinkItems = [
    {
      name: 'Youtube',
      url: 'www.youtube.com',
    },
    {
      name: 'Facebook',
      url: 'www.facebook.com',
    },
    {
      name: 'Twitter',
      url: 'www.twitter.com',
    },
  ];
  return (
    <div className="main-footer main-footer--white">
      <div className="container">
        <div className="row">
          <Links menuName="Overview" linkItems={overviewLinkItems} menuClassName="col-sm-3 menu" />
          <Links menuName="Menu" linkItems={menuLinkItems} menuClassName="col-sm-3 menu" />
          <Links menuName="Social" linkItems={socialLinkItems} menuClassName="col-sm-2 menu" />
          <Newsletter />
        </div>
        <Copyrights />
      </div>
    </div>
  );
}

export default Footer;
