import React from 'react';
import { Link } from 'react-router';

function Links(props) {
  const linkItems = props.linkItems.map((linkItem) => {
    if (linkItem.name !== 'Jobs') {
      return (
        <li key={linkItem.name}>
          <Link to={linkItem.url} >{linkItem.name}</Link>
        </li>
      );
    }
    return (
      <li key={linkItem.name}>
        <Link to={linkItem.url} >{linkItem.name}</Link>
        <Link to={linkItem.url} className="hiring">We are hiring!</Link>
      </li>
    );
  });
  return (
    <div className="text-left">
      <div className={props.menuClassName}>
        <h3>{props.menuName}</h3>
        <ul>
          {linkItems}
        </ul>
      </div>
    </div>
  );
}

Links.propTypes = {
  menuClassName: React.PropTypes.string.isRequired,
  menuName: React.PropTypes.string.isRequired,
  linkItems: React.PropTypes.string.isRequired,
};

export default Links;
