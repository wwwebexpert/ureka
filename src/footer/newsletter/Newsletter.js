import React from 'react';

function Newsletter() {
  return (
    <div>
      <div className="col-sm-4 newsletter">
        <div className="signup clearfix">
          <p>Sign up for the newsletter and we will inform you of updates, offers and more.</p>
          <form>
            <input
              type="text" name="email" className="form-control"
              placeholder="Your email address"
            />
            <input type="submit" value="Sign up" />
          </form>
        </div>
        <a href="www.twitter.com"><img src="images/social/social-tw.png" alt="twitter" /></a>
        <a href="www.dribble.com"><img src="images/social/social-dbl.png" alt="dribble" /></a>
      </div>
    </div>
  );
}

export default Newsletter;
