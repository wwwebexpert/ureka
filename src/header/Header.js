import React from 'react';

import MenuItem from './menuItem/MenuItem';

function Header() {
  return (
    <div>
      <header className="navbar navbar-inverse hero navbar-purple" role="banner">
        <div className="container">
          <div className="navbar-header">
            <button
              className="navbar-toggle" type="button" data-toggle="collapse"
              data-target=".bs-navbar-collapse"
            >
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a href="index.html" className="navbar-brand"><img
              src="images/logo_MJ.png"
              alt="mj-logo"
            /></a>
          </div>
          <nav className="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul className="nav navbar-nav navbar-right">
              <MenuItem menuClass="dropdown LtoR-white" menuUrl="lormi.html" menuName="Lormi" />
              <MenuItem menuClass="dropdown LtoR-white" menuUrl="ipsumi.html" menuName="Ipsumi" />
              <MenuItem menuClass="dropdown LtoR-white" menuUrl="resume.html" menuName="Resume" />

              <MenuItem menuClass="dropdown LtoR-white" menuUrl="signin.html" menuName="Sign In" />
              <MenuItem
                menuClass="dropdown active LtoR-white" menuUrl="signup.html"
                menuName="Sign Up"
              />
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
}

export default Header;
