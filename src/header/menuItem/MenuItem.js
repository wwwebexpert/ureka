import React from 'react';

function MenuItem(props) {
  return (
    <li className={props.menuClass}>
      <a
        href={props.menuUrl} className="dropdown-toggle"
        data-toggle="dropdown"
      >{props.menuName}</a>
    </li>
  );
}

MenuItem.propTypes = {
  menuUrl: React.PropTypes.string.isRequired,
  menuClass: React.PropTypes.string.isRequired,
  menuName: React.PropTypes.string.isRequired,
};

export default MenuItem;
