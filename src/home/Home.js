import React from 'react';

function Home() {
  return (
    <div>
      <div className="index-hero">
        <div className="container">
          <h1 className="hero-text animated fadeInDown text-center">Lorem Ipsum.
            <br />Lorem Ipsum. Lorem Ipsum.</h1>
          <div className="cta animated fadeInDown text-center">
            <a href="get-started.html" className="button">Get Started</a>
            <a href="documentation.html" className="button-outline">Documentation</a>
          </div>
          <img
            src="images/img_hero.jpg"
            className="img-responsive visible-lg-block" alt="home_img"
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
