import React from 'react';
import Plan from './plan/Plan';
import Header from './header/Header';

function Subscription() {
  return (
    <div>
      <div className="pricing-dark-section">
        <div className="container">
          <Header />
          <div className="row charts">
            <Plan
              planClassName="chart first" price="29" inverval="month"
              planName="Freelance" totalTeamMembers="5" totalProducts="10" transactionFee="1.0"
            />
            <Plan
              planClassName="chart featured" price="79" inverval="month"
              planName="Professional" totalTeamMembers="15" totalProducts="15" transactionFee="0.5"
            />
            <Plan
              planClassName="chart last" price="119" inverval="month"
              planName="Premium" totalTeamMembers="Unlimited" totalProducts="25" transactionFee="No"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Subscription;
