import React from 'react';

function Header() {
  return (
    <div>
      <div className="row header">
        <div className="col-md-12">
          <h3>Free trial. No contract. Cancel when you want.</h3>
          <p>All plans include a 7-day free trial</p>
        </div>
      </div>
    </div>
  );
}

export default Header;
