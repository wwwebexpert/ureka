import React from 'react';

function Plan(props) {
  let popularHtml = '';
  if (props.planClassName === 'chart featured') {
    popularHtml = <div className="popular">Most popular</div>;
  }
  return (
    <div>
      <div className="col-md-4">
        <div className={props.planClassName}>
          {popularHtml}
          <div className="quantity">
            <span className="dollar">$</span>
            <span className="price">{props.price}</span>
            <span className="period">/{props.inverval}</span>
          </div>
          <div className="plan-name">{props.planName}</div>
          <div className="specs">
            <div className="spec">
              <span className="variable">{props.totalTeamMembers}</span> team members
                </div>
            <div className="spec">
              <span className="variable">Digital</span> recurring billing
                </div>
            <div className="spec">
              <span className="variable">Virtual</span> online terminal
                </div>
            <div className="spec">
              <span className="variable">{props.totalProducts}</span> total products
                </div>
            <div className="spec">
              <span className="variable">{props.transactionFee}%</span> Transaction fee
                </div>
          </div>
          <a className="btn-signup button-clear" href="signup.html">
            <span>Start free trial</span>
          </a>
        </div>
      </div>
    </div>
  );
}

Plan.propTypes = {
  planClassName: React.PropTypes.string.isRequired,
  price: React.PropTypes.string.isRequired,
  inverval: React.PropTypes.string.isRequired,
  planName: React.PropTypes.string.isRequired,
  totalTeamMembers: React.PropTypes.string.isRequired,
  totalProducts: React.PropTypes.string.isRequired,
  transactionFee: React.PropTypes.string.isRequired,
};

export default Plan;
