import React from 'react';

import FeatureData from './featureData/FeatureData';
import Testimonials from './testimonials/Testimonials';
import Trial from './trial/Trial';

function ThemeFeatures() {
  return (
    <div>
      <div className="slider-section">
        <div className="container">
          <FeatureData heading="Includes all pages that a complete theme should have" />
        </div>
      </div>
      <Testimonials />
      <Trial />
    </div>
  );
}

export default ThemeFeatures;
