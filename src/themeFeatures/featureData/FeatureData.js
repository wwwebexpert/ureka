import React from 'react';

function FeatureData(props) {
  return (
    <div>
      <div className="row header">
        <div className="col-md-12">
          <h3>{props.heading}</h3>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12 slide-wrapper">
          <div className="slideshow">
            <div className="btn-nav prev" />
            <div className="btn-nav next" />
            <div className="slide active">
              <img src="images/slider/slide3.png" alt="slide3" />
            </div>
            <div className="slide">
              <img src="images/slider/slide4.png" alt="slide4" />
            </div>
            <div className="slide">
              <img src="images/slider/slide1.png" alt="slide1" />
            </div>
            <div className="slide">
              <img src="images/slider/slide5.png" alt="slide5" />
            </div>
            <div className="slide">
              <img src="images/slider/slide2.png" alt="slide2" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

FeatureData.propTypes = {
  heading: React.PropTypes.string.isRequired,
};
export default FeatureData;
