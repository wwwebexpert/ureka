import React from 'react';

function Testimonials() {
  const userReviewText1 = 'I am just quoting some stuff but I am seriously happy about this product. ';
  const userReviewText2 = ' Has a lot of powerful features and is so easy to set up, ';
  const userReviewText3 = "I could stay customizing it day and night, it's just so much fun!";
  const urt1 = userReviewText1 + userReviewText2 + userReviewText3;
  const urt2 = 'This thing is one of those tools that everybody should be using.' +
  'I really like it and with this ease to use,' +
  'you can kickstart your projects and apps and just focus on your business!';
  return (
    <div>
      <div className="testimonials-section">
        <div className="container">
          <TestimonialHeader />
          <div className="row">
            <UserTestimonial
              divClassName="testimonial pull-right" userReview={urt1}
              userImg="images/testimonials/testimonial3.jpg"
              userName="John McClane" userCompany="Microsoft"
            />
            <UserTestimonial
              divClassName="testimonial" userReview={urt2}
              userImg="images/testimonials/testimonial2.jpg"
              userName="Karen Jones" userCompany="Pixar Co."
            />
          </div>
        </div>
      </div>
    </div>
  );
}

function TestimonialHeader() {
  return (
    <div>
      <div className="row header">
        <div className="col-md-12">
          <h3>Trusted by a lot businesses around the world:</h3>
        </div>
      </div>
    </div>
  );
}

function UserTestimonial(props) {
  return (
    <div>
      <div className="col-sm-6">
        <div className={props.divClassName}>
          <div className="quote">
            {props.userReview}
            <div className="arrow-down">
              <div className="arrow" />
              <div className="arrow-border" />
            </div>
          </div>
          <div className="author">
            <img src={props.userImg} className="pic" alt={props.userName} />
            <div className="name">{props.userName}</div>
            <div className="company">{props.userCompany}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

UserTestimonial.propTypes = {
  userImg: React.PropTypes.string.isRequired,
  userName: React.PropTypes.string.isRequired,
  userCompany: React.PropTypes.string.isRequired,
  divClassName: React.PropTypes.string.isRequired,
  userReview: React.PropTypes.string.isRequired,
};

export default Testimonials;
