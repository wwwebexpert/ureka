import React from 'react';


function Trial() {
  return (
    <div>
      <div className="cta-section">
        <p>Start your free 14 day trial!</p>
        <a href="signup.html">Sign up for free</a>
      </div>
    </div>
  );
}

export default Trial;
